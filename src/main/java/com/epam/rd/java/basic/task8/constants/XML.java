package com.epam.rd.java.basic.task8.constants;

public enum XML {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    GROWINGTIPS("growingTips"),
    TEMPERATURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying"),

    MEASURE("measure"),
    LIGHTREQUIRING("lightRequiring"),

    XMLNS("xmlns"),
    XMLNS_XSI("xmlns:xsi"),
    XSI_SCHEMALOCATION("xsi:schemaLocation");

    private final String value;

    XML(final String value) {
        this.value = value;
    }

    /**
     * Determines if a name is equal to the string value wrapped by this enum element.<br/>
     * If a SAX/StAX parser make all names of elements and attributes interned you can use
     * <pre>return value == name;</pre> instead <pre>return value.equals(name);</pre>
     *
     * @param name string to compare with value.
     * @return value.equals(name)
     */
    public boolean equalsTo(final String name) {
        return value.equals(name);
    }

    public String value() {
        return value;
    }
}
