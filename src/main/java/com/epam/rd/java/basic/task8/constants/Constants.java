package com.epam.rd.java.basic.task8.constants;

public final class Constants {
	
	public static final String VALID_XML_FILE = "input.xml";
	public static final String INVALID_XML_FILE = "invalidXML.xml";
	public static final String XSD_FILE = "input.xsd";

	public static final String ATTRIBUTE_XMLNS ="http://www.nure.ua";
	public static final String ATTRIBUTE_XMLNS_XSI="http://www.w3.org/2001/XMLSchema-instance";
	public static final String ATTRIBUTE_XSI_SCHEMALOCATION="http://www.nure.ua input.xsd ";

	public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
	public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";

}