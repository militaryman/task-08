package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;

    private String currentElement;

    private Flowers flowers;

    private Flower flower;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Returns current Flowers
     *
     * @return Flowers
     */
    public Flowers getFlowers() {
        return flowers;
    }

    /**
     * Parses XML document.
     *
     * @param validate If true validate XML document against its XML schema.
     *                 With this parameter it is possible make parser validating.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void parse(final boolean validate)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

        saxParserFactory.setNamespaceAware(true);

        if (validate) {
            saxParserFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            saxParserFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        SAXParser saxParser = saxParserFactory.newSAXParser();
        saxParser.parse(xmlFileName, this);
    }

    @Override
    public void error(final org.xml.sax.SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName,
                             final Attributes attributes) {
        currentElement = localName;

        if (XML.FLOWERS.equalsTo(currentElement)) {
            flowers = new Flowers();
            return;
        }

        if (XML.FLOWER.equalsTo(currentElement)) {
            flower = new Flower();
            return;
        }

        if (XML.VISUALPARAMETERS.equalsTo(currentElement)) {
            flower.setVisualParameters(new VisualParameters());
            return;
        }

        if (XML.GROWINGTIPS.equalsTo(currentElement)) {
            flower.setGrowingTips(new GrowingTips());
            return;
        }

        if (XML.AVELENFLOWER.equalsTo(currentElement)) {
            flower.getVisualParameters().setAveLenFlower(new AveLenFlower());
            if (attributes.getLength() > 0) {
                flower.getVisualParameters().getAveLenFlower().setMeasure(attributes.getValue(0));
            }
            return;
        }

        if (XML.TEMPERATURE.equalsTo(currentElement)) {
            flower.getGrowingTips().setTempreture(new Tempreture());
            if (attributes.getLength() > 0) {
                flower.getGrowingTips().getTempreture().setMeasure(attributes.getValue(0));
            }
            return;
        }

        if (XML.LIGHTING.equalsTo(currentElement)) {
            flower.getGrowingTips().setLighting(new Lighting());
            if (attributes.getLength() > 0) {
                flower.getGrowingTips().getLighting().setLightRequiring(attributes.getValue(0));
            }
            return;
        }

        if (XML.WATERING.equalsTo(currentElement)) {
            flower.getGrowingTips().setWatering(new Watering());
            if (attributes.getLength() > 0) {
                flower.getGrowingTips().getWatering().setMeasure(attributes.getValue(0));
            }
            return;
        }
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) {
        String elemText = new String(ch, start, length).trim();

        if (elemText.isEmpty()) {
            return;
        }

        if (XML.NAME.equalsTo(currentElement)) {
            flower.setName(elemText);
            return;
        }

        if (XML.SOIL.equalsTo(currentElement)) {
            flower.setSoil(Soil.fromValue(elemText));
            return;
        }

        if (XML.ORIGIN.equalsTo(currentElement)) {
            flower.setOrigin(elemText);
            return;
        }

        if (XML.STEMCOLOUR.equalsTo(currentElement)) {
            flower.getVisualParameters().setStemColour(elemText);
            return;
        }

        if (XML.LEAFCOLOUR.equalsTo(currentElement)) {
            flower.getVisualParameters().setLeafColour(elemText);
            return;
        }

        if (XML.AVELENFLOWER.equalsTo(currentElement)) {
            flower.getVisualParameters().getAveLenFlower().setValue(new BigInteger(elemText));
            return;
        }

        if (XML.TEMPERATURE.equalsTo(currentElement)) {
            flower.getGrowingTips().getTempreture().setValue(new BigInteger(elemText));
            return;
        }

        if (XML.WATERING.equalsTo(currentElement)) {
            flower.getGrowingTips().getWatering().setValue(new BigInteger(elemText));
            return;
        }

        if (XML.MULTIPLYING.equalsTo(currentElement)) {
            flower.setMultiplying(Multiplying.fromValue(elemText));
            return;
        }
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) {
        if (XML.FLOWER.equalsTo(localName)) {
            flowers.getFlower().add(flower);
            return;
        }
    }
}