package com.epam.rd.java.basic.task8.util;

import java.util.Comparator;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;

public class Sorter {

    public static final Comparator<Flower> SORT_FLOWERS_BY_NAME = (o1, o2) -> o1.getName().compareTo(o2.getName());

    public static final Comparator<Flower> SORT_FLOWERS_BY_AVELENFLOWER = (o1, o2) ->
            o2.getVisualParameters().getAveLenFlower().getValue().compareTo(o1.getVisualParameters().getAveLenFlower().getValue());

    public static final Comparator<Flower> SORT_FLOWERS_BY_WATERING = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return o1.getGrowingTips().getWatering().getValue().compareTo(o2.getGrowingTips().getWatering().getValue());
        }
    };

    public static void sortFlowerByName(Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_NAME);
    }

    public static void setSortFlowerByAvelenflower(Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_AVELENFLOWER);
    }

    public static void sortFalowerByWatering(Flowers flowers) {
        flowers.getFlower().sort(SORT_FLOWERS_BY_WATERING);
    }

}
