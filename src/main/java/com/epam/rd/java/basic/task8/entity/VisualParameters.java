package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stemColour"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="([a-zA-Zа-яА-Я])+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="leafColour"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="([a-zA-Zа-яА-Я])+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="aveLenFlower" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;positiveInteger"&gt;
 *                 &lt;attribute name="measure" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="cm" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "stemColour",
        "leafColour",
        "aveLenFlower"
})
public class VisualParameters {

    @XmlElement(required = true)
    protected String stemColour;
    @XmlElement(required = true)
    protected String leafColour;
    protected AveLenFlower aveLenFlower;

    /**
     * Gets the value of the stemColour property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStemColour() {
        return stemColour;
    }

    /**
     * Sets the value of the stemColour property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStemColour(String value) {
        this.stemColour = value;
    }

    /**
     * Gets the value of the leafColour property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLeafColour() {
        return leafColour;
    }

    /**
     * Sets the value of the leafColour property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLeafColour(String value) {
        this.leafColour = value;
    }

    /**
     * Gets the value of the aveLenFlower property.
     *
     * @return possible object is
     * {@link //Flowers.Flower.VisualParameters.AveLenFlower }
     */
    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    /**
     * Sets the value of the aveLenFlower property.
     *
     * @param value allowed object is
     *              {@link //Flowers.Flower.VisualParameters.AveLenFlower }
     */
    public void setAveLenFlower(AveLenFlower value) {
        this.aveLenFlower = value;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}