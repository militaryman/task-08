package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "multiplying")
@XmlEnum
public enum Multiplying {
    @XmlEnumValue("листья")
    MULTIPLYING1("листья"),
    @XmlEnumValue("черенки")
    MULTIPLYING2("черенки"),
    @XmlEnumValue("семена")
    MULTIPLYING3("семена");
    private final String value;

    Multiplying(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static String fromValue(String v) {
        for (Multiplying c : Multiplying.values()) {
            if (c.value.equals(v)) {
                return c.value;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
