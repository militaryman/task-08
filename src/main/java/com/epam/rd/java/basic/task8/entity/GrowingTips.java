package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tempreture"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;positiveInteger"&gt;
 *                 &lt;attribute name="measure" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="celcius" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="lighting"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="lightRequiring" use="required"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;enumeration value="yes"/&gt;
 *                       &lt;enumeration value="no"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="watering" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;positiveInteger"&gt;
 *                 &lt;attribute name="measure" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="mlPerWeek" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tempreture",
        "lighting",
        "watering"
})
public class GrowingTips {

    @XmlElement(required = true)
    protected Tempreture tempreture;
    @XmlElement(required = true)
    protected Lighting lighting;
    protected Watering watering;

    /**
     * Gets the value of the tempreture property.
     *
     * @return possible object is
     * {@link //Flowers.Flower.GrowingTips.Tempreture }
     */
    public Tempreture getTempreture() {
        return tempreture;
    }

    /**
     * Sets the value of the tempreture property.
     *
     * @param value allowed object is
     *              {@link //Flowers.Flower.GrowingTips.Tempreture }
     */
    public void setTempreture(Tempreture value) {
        this.tempreture = value;
    }

    /**
     * Gets the value of the lighting property.
     *
     * @return possible object is
     * {@link //Flowers.Flower.GrowingTips.Lighting }
     */
    public Lighting getLighting() {
        return lighting;
    }

    /**
     * Sets the value of the lighting property.
     *
     * @param value allowed object is
     *              {@link //Flowers.Flower.GrowingTips.Lighting }
     */
    public void setLighting(Lighting value) {
        this.lighting = value;
    }

    /**
     * Gets the value of the watering property.
     *
     * @return possible object is
     * {@link //Flowers.Flower.GrowingTips.Watering }
     */
    public Watering getWatering() {
        return this.watering;
    }

    /**
     * Sets the value of the watering property.
     *
     * @param value allowed object is
     *              {@link //Flowers.Flower.GrowingTips.Watering }
     */
    public void setWatering(Watering value) {
        this.watering = value;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}