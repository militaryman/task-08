package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    private Flowers flowers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Returns current Flowers
     *
     * @return Flowers
     */
    public Flowers getFlowers() {
        return flowers;
    }

    /**
     * Parses XML document.
     *
     * @param validate  If true validate XML document against its XML schema.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void parse(final boolean validate) throws ParserConfigurationException,
            SAXException, IOException {

        DocumentBuilderFactory docBuilerFactory = DocumentBuilderFactory.newInstance();

        docBuilerFactory.setNamespaceAware(true);

        if (validate) {
            docBuilerFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);

            docBuilerFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        DocumentBuilder docBuilder = docBuilerFactory.newDocumentBuilder();

        docBuilder.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });

        Document document = docBuilder.parse(xmlFileName);
        Element rootElem = document.getDocumentElement();
        flowers = new Flowers();

        NodeList flowersNodes = rootElem.getElementsByTagName(XML.FLOWER.value());

        for (int j = 0; j < flowersNodes.getLength(); j++) {
            Flower flower = getFlower(flowersNodes.item(j));
            flowers.getFlower().add(flower);
        }
    }

    /**
     * Extracts Flower object from the Flower XML node.
     *
     * @param node
     * @return Flower
     */
    private Flower getFlower(final Node node) {
        Flower flower = new Flower();
        Element elem = (Element) node;

        Node dataNode = elem.getElementsByTagName(XML.NAME.value()).item(0);
        flower.setName(dataNode.getTextContent());

        dataNode = elem.getElementsByTagName(XML.SOIL.value()).item(0);
        flower.setSoil(Soil.fromValue(dataNode.getTextContent()));

        dataNode = elem.getElementsByTagName(XML.ORIGIN.value()).item(0);
        flower.setOrigin(dataNode.getTextContent());

        dataNode = elem.getElementsByTagName(XML.VISUALPARAMETERS.value()).item(0);
        flower.setVisualParameters(getVisualParameters(dataNode));

        dataNode = elem.getElementsByTagName(XML.GROWINGTIPS.value()).item(0);
        flower.setGrowingTips(getGrowingTips(dataNode));

        dataNode = elem.getElementsByTagName(XML.MULTIPLYING.value()).item(0);
        flower.setMultiplying(Multiplying.fromValue(dataNode.getTextContent()));

        return flower;
    }

    /**
     * Extracts GrowingTips object from the GrowingTips XML node.
     *
     * @param node
     * @return GrowingTips
     */
    private GrowingTips getGrowingTips(final Node node) {
        GrowingTips growingTips = new GrowingTips();
        Element elem = (Element) node;

        Node dataNode = elem.getElementsByTagName(XML.TEMPERATURE.value()).item(0);
        growingTips.setTempreture(getTemperature(dataNode));

        dataNode = elem.getElementsByTagName(XML.LIGHTING.value()).item(0);
        growingTips.setLighting(getLighting(dataNode));

        dataNode = elem.getElementsByTagName(XML.WATERING.value()).item(0);
        growingTips.setWatering(getWatering(dataNode));

        return growingTips;
    }

    /**
     * Extracts Watering object from the Watering XML node.
     *
     * @param node
     * @return Watering
     */
    private Watering getWatering(final Node node) {
        Watering watering = new Watering();

        watering.setValue(new BigInteger(node.getTextContent()));

        watering.setMeasure(node.getAttributes().item(0).getTextContent());

        return watering;
    }

    /**
     * Extracts Lighting object from the Lighting XML node.
     *
     * @param node
     * @return Lighting
     */
    private Lighting getLighting(final Node node) {
        Lighting lighting = new Lighting();

        lighting.setLightRequiring(node.getAttributes().item(0).getTextContent());

        return lighting;
    }

    /**
     * Extracts Temperature object from the Temperature XML node.
     *
     * @param node
     * @return Temperature
     */
    private Tempreture getTemperature(final Node node) {
        Tempreture tempreture = new Tempreture();

        tempreture.setValue(new BigInteger(node.getTextContent()));

        tempreture.setMeasure(node.getAttributes().item(0).getTextContent());

        return tempreture;
    }

    /**
     * Extracts VisualParameters object from the VisualParameters XML node.
     *
     * @param node
     * @return VisualParameters
     */
    private VisualParameters getVisualParameters(final Node node) {
        VisualParameters visualParameters = new VisualParameters();
        Element elem = (Element) node;

        Node dataNode = elem.getElementsByTagName(XML.STEMCOLOUR.value()).item(0);
        visualParameters.setStemColour(dataNode.getTextContent());

        dataNode = elem.getElementsByTagName(XML.LEAFCOLOUR.value()).item(0);
        visualParameters.setLeafColour(dataNode.getTextContent());

        dataNode = elem.getElementsByTagName(XML.AVELENFLOWER.value()).item(0);
        visualParameters.setAveLenFlower(getAveLenFlower(dataNode));

        return visualParameters;
    }

    /**
     * Extracts AveLenFlower object from the AveLenFlower XML node.
     *
     * @param node
     * @return AveLenFlower
     */
    private AveLenFlower getAveLenFlower(final Node node) {
        AveLenFlower aveLenFlower = new AveLenFlower();

        aveLenFlower.setValue(new BigInteger(node.getTextContent()));

        aveLenFlower.setMeasure(node.getAttributes().item(0).getTextContent());

        return aveLenFlower;
    }

    /**
     * Creates XML document to save it from Flowers.
     *
     * @param flowers
     * @return Document
     * @throws ParserConfigurationException
     */
    public static Document getDocument(final Flowers flowers)
            throws ParserConfigurationException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

        docBuilderFactory.setNamespaceAware(true);

        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        doc.setXmlVersion("1.0");
        doc.setXmlStandalone(true);

        Element textElem = doc.createElement(XML.FLOWERS.value());
        textElem.setAttribute(XML.XMLNS.value(), Constants.ATTRIBUTE_XMLNS);
        textElem.setAttribute(XML.XMLNS_XSI.value(), Constants.ATTRIBUTE_XMLNS_XSI);
        textElem.setAttribute(XML.XSI_SCHEMALOCATION.value(), Constants.ATTRIBUTE_XSI_SCHEMALOCATION);

        doc.appendChild(textElem);

        for (Flower flower : flowers.getFlower()) {

            Element elem = doc.createElement(XML.FLOWER.value());
            textElem.appendChild(elem);

            Element flowerElement = doc.createElement(XML.NAME.value());
            flowerElement.setTextContent(flower.getName());
            elem.appendChild(flowerElement);

            flowerElement = doc.createElement(XML.SOIL.value());
            flowerElement.setTextContent(flower.getSoil());
            elem.appendChild(flowerElement);

            flowerElement = doc.createElement(XML.ORIGIN.value());
            flowerElement.setTextContent(flower.getOrigin());
            elem.appendChild(flowerElement);

            flowerElement = doc.createElement(XML.VISUALPARAMETERS.value());

            Element visualParametersElement = doc.createElement(XML.STEMCOLOUR.value());
            visualParametersElement.setTextContent(flower.getVisualParameters().getStemColour());
            flowerElement.appendChild(visualParametersElement);

            visualParametersElement = doc.createElement(XML.LEAFCOLOUR.value());
            visualParametersElement.setTextContent(flower.getVisualParameters().getLeafColour());
            flowerElement.appendChild(visualParametersElement);

            visualParametersElement = doc.createElement(XML.AVELENFLOWER.value());
            visualParametersElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
            visualParametersElement.setAttribute(XML.MEASURE.value(), flower.getVisualParameters().getAveLenFlower().getMeasure());
            flowerElement.appendChild(visualParametersElement);

            elem.appendChild(flowerElement);

            flowerElement = doc.createElement(XML.GROWINGTIPS.value());

            Element growingTipsElement = doc.createElement(XML.TEMPERATURE.value());
            growingTipsElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
            growingTipsElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getTempreture().getMeasure());
            flowerElement.appendChild(growingTipsElement);

            growingTipsElement = doc.createElement(XML.LIGHTING.value());
            growingTipsElement.setAttribute(XML.LIGHTREQUIRING.value(), flower.getGrowingTips().getLighting().getLightRequiring());
            flowerElement.appendChild(growingTipsElement);

            growingTipsElement = doc.createElement(XML.WATERING.value());
            growingTipsElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
            growingTipsElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getWatering().getMeasure());
            flowerElement.appendChild(growingTipsElement);

            elem.appendChild(flowerElement);

            flowerElement = doc.createElement(XML.MULTIPLYING.value());
            flowerElement.setTextContent(flower.getMultiplying());
            elem.appendChild(flowerElement);
        }

        return doc;
    }

    /**
     * Saves Test object to XML file.
     *
     * @param flowers
     * @param xmlFileName
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static void saveToXML(final Flowers flowers, final String xmlFileName)
            throws ParserConfigurationException, TransformerException {
        saveToXML(getDocument(flowers), xmlFileName);
    }

    /**
     * Save DOM to XML.
     *
     * @param document
     * @param xmlFileName
     * @throws TransformerException
     */
    public static void saveToXML(final Document document, final String xmlFileName)
            throws TransformerException {

        StreamResult res = new StreamResult(new File(xmlFileName));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        transformer.transform(new DOMSource(document), res);
    }
}
