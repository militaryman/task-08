package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "soil")
@XmlEnum
public enum Soil {
    @XmlEnumValue("подзолистая")
    SOIL1("подзолистая"),
    @XmlEnumValue("грунтовая")
    SOIL2("грунтовая"),
    @XmlEnumValue("дерново-подзолистая")
    SOIL3("дерново-подзолистая");
    private final String value;

    Soil(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static String fromValue(String v) {
        for (Soil c : Soil.values()) {
            if (c.value.equals(v)) {
                return c.value;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
