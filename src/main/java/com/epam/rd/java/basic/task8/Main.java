package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.util.Sorter;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        if (args.length != 1) {
            return;
        }

        String xmlFileName = "input.xml";
        System.out.println("Input ==> " + xmlFileName);
        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////
        // get container
        DOMController domController = new DOMController(xmlFileName);

        try {
            domController.parse(true);
        } catch (DOMException | ParserConfigurationException | SAXException
                | IOException e) {
            System.err.println(e.getMessage());
        }
        Flowers flowers = domController.getFlowers();
        System.out.println(flowers);

        // sort (case 1)
        Sorter.sortFlowerByName(flowers);

        // save
        String outputXmlFile = "output.dom.xml";

        try {
            DOMController.saveToXML(flowers, outputXmlFile);
        } catch (DOMException | ParserConfigurationException
                | TransformerException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Output ==> " + outputXmlFile);
        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////
        // get
        SAXController saxController = new SAXController(xmlFileName);

        try {
            saxController.parse(true);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println(e.getMessage());
        }
        flowers = saxController.getFlowers();
        System.out.println(flowers);

        // sort  (case 2)
        Sorter.setSortFlowerByAvelenflower(flowers);

        // save
        outputXmlFile = "output.sax.xml";

        try {
            DOMController.saveToXML(flowers, outputXmlFile);
        } catch (DOMException | ParserConfigurationException
                | TransformerException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Output ==> " + outputXmlFile);
        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////
        // get
        STAXController staxController = new STAXController(xmlFileName);

        try {
            staxController.parse();
        } catch (ParserConfigurationException | SAXException | IOException
                | XMLStreamException e) {
            System.err.println(e.getMessage());
        }
        flowers = staxController.getFlowers();
        System.out.println(flowers);

        // sort  (case 3)
        Sorter.sortFalowerByWatering(flowers);

        // save
        outputXmlFile = "output.stax.xml";

        try {
            DOMController.saveToXML(flowers, outputXmlFile);
        } catch (DOMException | ParserConfigurationException
                | TransformerException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Output ==> " + outputXmlFile);
    }
}
