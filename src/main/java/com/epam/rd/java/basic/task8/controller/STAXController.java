package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    private Flowers flowers;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Returns current Flowers
     *
     * @return Flowers
     */
    public Flowers getFlowers() {
        return flowers;
    }

    /**
     * Parses XML document with StAX (based on event reader). There is no validation during the parsing.
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XMLStreamException
     */
    public void parse() throws ParserConfigurationException, SAXException,
            IOException, XMLStreamException {
        Flower flower = null;

        String currentElement = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(
                new StreamSource(xmlFileName));

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();

            if (xmlEvent.isCharacters() && xmlEvent.asCharacters().isWhiteSpace()) {
                continue;
            }

            if (xmlEvent.isStartElement()) {
                StartElement startElem = xmlEvent.asStartElement();
                currentElement = startElem.getName().getLocalPart();

                if (XML.FLOWERS.equalsTo(currentElement)) {
                    flowers = new Flowers();
                    continue;
                }

                if (XML.FLOWER.equalsTo(currentElement)) {
                    flower = new Flower();
                    continue;
                }

                if (XML.VISUALPARAMETERS.equalsTo(currentElement)) {
                    flower.setVisualParameters(new VisualParameters());
                    continue;
                }

                if (XML.GROWINGTIPS.equalsTo(currentElement)) {
                    flower.setGrowingTips(new GrowingTips());
                    continue;
                }

                if (XML.AVELENFLOWER.equalsTo(currentElement)) {
                    flower.getVisualParameters().setAveLenFlower(new AveLenFlower());
                    flower.getVisualParameters().getAveLenFlower().setMeasure(
                            startElem.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
                    continue;
                }

                if (XML.TEMPERATURE.equalsTo(currentElement)) {
                    flower.getGrowingTips().setTempreture(new Tempreture());
                    flower.getGrowingTips().getTempreture().setMeasure(
                            startElem.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
                    continue;
                }

                if (XML.LIGHTING.equalsTo(currentElement)) {
                    flower.getGrowingTips().setLighting(new Lighting());
                    flower.getGrowingTips().getLighting().setLightRequiring(
                            startElem.getAttributeByName(new QName(XML.LIGHTREQUIRING.value())).getValue());
                    continue;
                }

                if (XML.WATERING.equalsTo(currentElement)) {
                    flower.getGrowingTips().setWatering(new Watering());
                    flower.getGrowingTips().getWatering().setMeasure(
                            startElem.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
                    continue;
                }
            }

            if (xmlEvent.isCharacters()) {
                Characters characters = xmlEvent.asCharacters();
                String elemText = characters.getData();

                if (XML.NAME.equalsTo(currentElement)) {
                    flower.setName(characters.getData());
                    continue;
                }

                if (XML.SOIL.equalsTo(currentElement)) {
                    flower.setSoil(Soil.fromValue(elemText));
                    continue;
                }

                if (XML.ORIGIN.equalsTo(currentElement)) {
                    flower.setOrigin(elemText);
                    continue;
                }

                if (XML.STEMCOLOUR.equalsTo(currentElement)) {
                    flower.getVisualParameters().setStemColour(elemText);
                    continue;
                }

                if (XML.LEAFCOLOUR.equalsTo(currentElement)) {
                    flower.getVisualParameters().setLeafColour(elemText);
                    continue;
                }

                if (XML.AVELENFLOWER.equalsTo(currentElement)) {
                    flower.getVisualParameters().getAveLenFlower().setValue(new BigInteger(elemText));
                    continue;
                }

                if (XML.TEMPERATURE.equalsTo(currentElement)) {
                    flower.getGrowingTips().getTempreture().setValue(new BigInteger(elemText));
                    continue;
                }

                if (XML.WATERING.equalsTo(currentElement)) {
                    flower.getGrowingTips().getWatering().setValue(new BigInteger(elemText));
                    continue;
                }

                if (XML.MULTIPLYING.equalsTo(currentElement)) {
                    flower.setMultiplying(Multiplying.fromValue(elemText));
                    continue;
                }
            }

            if (xmlEvent.isEndElement()) {
                EndElement endElem = xmlEvent.asEndElement();
                String localName = endElem.getName().getLocalPart();

                if (XML.FLOWER.equalsTo(localName)) {
                    flowers.getFlower().add(flower);
                    continue;
                }
            }
        }
        xmlEventReader.close();
    }
}
